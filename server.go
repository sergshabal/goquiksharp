package main

// https://godoc.org/github.com/BurntSushi/toml#example-Decode
import (
	"errors"
	"fmt"
)

type ServerConfig struct {
	Ports       []int
	Description string
	AccessRuls  []string
}

type Server struct {
	IP     string       `toml:"ip"`
	Config ServerConfig `toml:"config"`
}

type Servers map[string]Server

func (s *Server) GetReqAddr() (string, error) {
	return s.getAddr(0)
}

func (s *Server) GetCallbAddr() (string, error) {
	return s.getAddr(1)
}

func (s *Server) getAddr(portType int) (string, error) {
	if len(s.IP) <= 0 {
		return "", errors.New("wrorn server IP")
	}
	if len(s.Config.Ports) < 2 {
		return "", errors.New("wrorn server ports count")
	}
	return fmt.Sprintf("%s:%d", s.IP, s.Config.Ports[portType]), nil
}
