package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	// "io"
	// "log"
	"os"
	// "strings"
)

type Quote struct {
	Price json.Number `json:"price"`
	Qty   json.Number `json:"quantity"`
}

type QuoteData struct {
	Seccode    string      `json:"sec_code"`
	Classcode  string      `json:"class_code"`
	OfferCount json.Number `json:"offer_count"`
	BidCount   json.Number `json:"bid_count"`
	Offers     []Quote     `json:"offer"`
	Bids       []Quote     `json:"bid"`
	ServerTime string      `json:"server_time"`
}

type QuikSharpMessage struct {
	Id int64
	// Data map[string]interface{} //QuoteData
	// Data QuoteData
	Data json.RawMessage
	Cmd  string
	T    float64
	V    string
}

func main1() {
	file, err := os.Open("./receive.txt")
	if err != nil {
		panic("err open")
	}
	defer file.Close()

	reader := bufio.NewReader(file)
	scanner := bufio.NewScanner(reader)

	// dec := json.NewDecoder(strings.NewReader(jsonStream))

	for scanner.Scan() {
		var m QuikSharpMessage
		json.Unmarshal([]byte(scanner.Text()), &m)

		fmt.Println("message: ", m.Cmd, m.Id, m.T, m.V)
		var dst interface{}
		switch m.Cmd {
		case "OnQuote":
			dst = new(QuoteData)
		default:
			{
				fmt.Println("unknown msg")
				continue
			}
		}
		json.Unmarshal(m.Data, dst)
		ob := (dst.(*QuoteData))
		fmt.Println(ob.Seccode)
		bc, err := ob.BidCount.Float64()
		if err != nil {
			panic("BidCount parse error")
		}
		fmt.Println(bc)
		// fmt.Println(string(m.Data))
		// fmt.Println("convert data to int", m.Data.BidCount, )
		// for k, v := range m.Data {
		// 	fmt.Println(k)
		// 	if k == "offer_count" {
		// 		fmt.Println(v, v.Float64()+1.0)
		// 	}
		// }
		// fmt.Printf("bisd = %v \noffers = %v\n", m.Data.Bids, m.Data.Offers)
		// fmt.Printf("\n")
	}

}
