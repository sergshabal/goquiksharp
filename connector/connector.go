package connector

import (
	"log"
	"net"
)

type ConnectInterface interface {
	Connect() (bool, error)
	Disconnect() (bool, error)
	isConnect() bool
}

type Connector struct {
	RequestChan   chan<- string
	ReplyChan     <-chan string
	logger        *log.Logger
	isConnect     bool
	reqAddr       string
	request_conn  *net.Conn
	callbAddr     string
	callback_conn *net.Conn
}

func (c *Connector) Connect() (bool, error) {
	c.logger.Println("try connect to ", c.reqAddr, c.callbAddr)
	// return true, nil
	callback_conn, err := net.Dial("tcp", c.callbAddr)
	if err != nil {
		c.logger.Println("error on connection")
		c.logger.Println(err.Error())
		return false, err
	}
	req_conn, err := net.Dial("tcp", c.reqAddr)
	if err != nil {
		c.logger.Println("error on connection")
		c.logger.Println(err.Error())
		return false, err
	}
	c.request_conn = &req_conn
	c.callback_conn = &callback_conn
	c.isConnect = true
	return c.isConnect, nil
}

func (c *Connector) CloseConnections() {
	close_conn := func(conn *net.Conn) {
		if conn != nil {
			remAddr := (*conn).RemoteAddr().String()
			c.logger.Printf("clossing %s ... ", remAddr)
			if err := (*conn).Close(); err != nil {
				c.logger.Printf("error.\n")
				c.logger.Println(err.Error())
			} else {
				c.logger.Printf("closed.\n")
			}
		}
	}

	close_conn(c.request_conn)
	close_conn(c.callback_conn)
}

func (c *Connector) SetReqAddr(addr string) {
	c.reqAddr = addr
}

func (c *Connector) SetCallbAddr(addr string) {
	c.callbAddr = addr
}

func NewConnector(loger *log.Logger) *Connector {
	return &Connector{
		logger:    loger,
		isConnect: false,
	}
}
