package main

import (
	"fmt"
	// "io/ioutil"
	"bufio"
	"log"
	"net"
	"os"
	"os/signal"
	"time"
)

const (
	port          = 34130
	callback_port = port + 1
)

var (
	reqChan chan string
)

func main2() {

	var m1 = fmt.Sprintf("localhost:%v", port)
	var m2 = fmt.Sprintf("localhost:%v", callback_port)
	fmt.Println("requests addr: ", m1)
	fmt.Println("callback addr: ", m2)

	callback_conn, err1 := net.Dial("tcp", m2)
	if err1 != nil {
		log.Println("listen", err1)
	} else {
		defer callback_conn.Close()
	}
	callbacks_receive := 0
	callbacks_err := 0

	go func() {
		f, ferr := os.Create("./receive.txt")
		if ferr != nil {
			panic("cant create file")
		}
		defer f.Close()

		eof_printed := false
		for {
			status, err := bufio.NewReader(callback_conn).ReadString('\n')
			// _, err := bufio.NewReader(callback_conn).ReadString('\n')

			if err != nil {
				callbacks_err++
				if !eof_printed {
					log.Println(err)
					eof_printed = err.Error() == "EOF"
				}
			} else {
				callbacks_receive++
				f.WriteString(status)
				// fmt.Println(status)
			}
		}
	}()

	req_receive := 0
	request_conn, err := net.Dial("tcp", m1)
	if err != nil {
		log.Println("dial", err)
	}
	defer request_conn.Close()
	// var t float64
	buff := make([]byte, 2048)
	msg_count := 10000
	t1 := time.Now()

	var reqChan = make(chan string)
	go getInfo(reqChan)

	go func(msgChan chan string) {
		// for i := 0; i < msg_count; i++ {
		for {
			// fmt.Printf("%.1f\n", t)
			// msg := fmt.Sprintf(`{"id":0, "data":"Ping", "cmd":"ping", "v":"", "t":%.1f}`, t)
			msg := <-msgChan
			fmt.Println(msg)
			// t = (float64)(time.Now().UnixNano()) / 1000000.0
			// fmt.Println("send to socket: ", msg)
			request_conn.Write([]byte(msg + "\n"))
			request_conn.Read(buff)
			req_receive++
			// n, _ := request_conn.Read(buff)
			// name := string(buff[0 : n-1])
			// fmt.Println(name, len(name))
			// time.Sleep(5 * time.Millisecond)
		}
	}(reqChan)

	// go func() {
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt)
	s := <-c
	fmt.Println("\n\nexit with", s)
	td := time.Since(t1)
	msg_count = req_receive
	fmt.Printf("milliseconds per loop %.3f ms/op\n",
		float64(td.Nanoseconds())/1000000.0/float64(msg_count))
	fmt.Printf("secconds for all %v sec\n", td.Seconds())
	fmt.Printf("request receive %v\n", req_receive)
	fmt.Printf("callbacks receive %v\n", callbacks_receive)
	fmt.Printf("callbacks err %v\n", callbacks_err)
	all_received := req_receive + callbacks_err + callbacks_receive
	fmt.Printf("milliseconds per all receives %.3f ms/op\n",
		float64(td.Nanoseconds())/1000000.0/float64(all_received))
	// os.Exit(1)
	// }()

	// time.Sleep(10 * time.Second)
}

func getInfo(msgChan chan string) {
	for {
		fmt.Println("getInfo")
		t := (float64)(time.Now().UnixNano()) / 1000000.0
		msg := fmt.Sprintf(`{"id":0, "data":"Ping", "cmd":"ping", "v":"", "t":%.1f}`, t)
		msgChan <- msg
		time.Sleep(5 * time.Millisecond)
	}
}
