package main

import (
	"fmt"
	"github.com/BurntSushi/toml"
	"goquiksharp/connector"
	"io"
	"log"
	"os"
	"os/signal"

	"time"
)

const (
	ServName          = "GoQuikServ"
	Version           = "0.1.0"
	setting_fname     = "./param.toml"
	def_port          = 34130
	def_callback_port = def_port + 1
)

var (
	Info      *log.Logger
	reqAddr   string
	callbAddr string
	config    Servers
)

func main() {
	// fname := time.Now().Format("20060102T150405.txt")
	fname := "logfile.txt"
	logf, err := os.OpenFile("./logs/"+fname, os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatalln(err)
		panic("error log file create")
	}
	defer logf.Close()
	multi := io.MultiWriter(logf, os.Stdout)
	Info := log.New(multi, "", log.Ldate|log.Ltime|log.Lshortfile)

	// read settings
	err = readSettings(setting_fname)
	if err != nil {
		Info.Println("error on reading settings file. Load default")
		panic("can't load settings")
	}

	go runGoQuikServ(Info)

	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt)
	s := <-c
	Info.Printf("exit with %s\n", s)
	Info.Printf("%s goin down.\n", ServName)
	fmt.Println("FINISH!")
}

func runGoQuikServ(info *log.Logger) {
	info.Printf("starting %s", ServName)
	conns := []*connector.Connector{}
	defer func() {
		for _, conn := range conns {
			conn.CloseConnections()
		}
	}()
	for name, _ := range config {
		sc := config[name]

		conn := connector.NewConnector(info)
		req, err := sc.GetReqAddr()
		if err != nil {
			info.Printf("Error on get Addr for '%s'\n", name)
			continue
		}
		conn.SetReqAddr(req)
		callb, err := sc.GetCallbAddr()
		if err != nil {
			info.Printf("Error on get Addr for '%s'\n", name)
			continue
		}
		conn.SetCallbAddr(callb)
		conns = append(conns, conn)
	}

	for _, con := range conns {
		con.Connect()
	}

	for i := 0; i < 3; i++ {
		time.Sleep(2 * time.Second)
		info.Println("timed out")
	}
}

func readSettings(fname string) error {
	_, err := toml.DecodeFile(fname, &config)
	return err
}
